# Math Runner

A minigame for the course "Advanced Experiment Design in Unity" taught by Farbod Nosrat Nezami, M. Sc., in Summer Semester 2021 of the University of Osnabrueck.

**Third party assets (retrieved through Unity's asset store):**
- Player Character and Running Animation: "Jammo Character" by Mix and Jam (https://assetstore.unity.com/packages/3d/characters/jammo-character-mix-and-jam-158456)
- Player Lives / Asteroids and Random Tumbling: "Asteroids Pack" by Mark Dion (https://assetstore.unity.com/packages/3d/environments/asteroids-pack-84988)
- Skybox / Space Background: "Dynamic Space Background Lite" (asset "Nebula Blue.png") by DinV Studio (https://assetstore.unity.com/packages/2d/textures-materials/dynamic-space-background-lite-104606)
- Skybox for ground material: "Deep Space Skybox Pack" (asset "galacticgreen_left.tif") by Sean Duffy (https://assetstore.unity.com/packages/2d/textures-materials/deep-space-skybox-pack-11056)
- Button Click Sound: "Minimal UI Sounds" (clip "Spacey Click_Minimal UI Sounds") by cabled_mess (https://assetstore.unity.com/packages/audio/sound-fx/minimal-ui-sounds-78266)
- Background Music: "Free Music Tracks For Games" (clip "Zephyr") by Rizwan Ashraf (https://assetstore.unity.com/packages/audio/music/free-music-tracks-for-games-156413) 

**_Things I would adjust while further working on the game:_**
- Make the game visually more interesting by adding floating objects along the way
- Add the possibility for subtraction tasks
- Find a more suitable background music
- Make the background, maybe even the whole theme selectable
- Think about including power ups (and whether this is reasonable for a game like this) -> more time between task presence and answer choice, incorrect answer does not subtract life, gain more lives etc.
- Include animations of some texts (game title, pause title, ...)
- Make the player character "react" to answers (happy for correct answer, sad for incorrect answer and game over)
