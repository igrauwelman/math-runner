﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manages the highscore
/// </summary>
public class HighScoreManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> _entries; // placeholder GameObjects for 1st to 4th place
    private Dictionary<int, List<string>> _entryDictionary; // saved highscore entries (int = place, List = [name, score])
    private List<string> _data = new List<string>(2); // will save value of specific key in _entryDictionary

    private void Awake()
    {
        // get saved dictionary
        _entryDictionary = GameManager.Instance.highscoreEntries;
        
        // set as many entries active as there are scores saved in the dictionary
        for (var i = 0; i < _entryDictionary.Count; i++)
        {
            _entries[i].SetActive(true);
            _entries[i].transform.GetChild(0).GetComponent<Text>().text = _entryDictionary[i][0];
            _entries[i].transform.GetChild(1).GetComponent<Text>().text = _entryDictionary[i][1];
        }
        
        // if a new entry is saved, add it to the dictionary
        if (!GameManager.Instance.newNameAndScoreSaved) return;
        AddEntry(GameManager.Instance.playerName, GameManager.Instance.playerScore);
        GameManager.Instance.newNameAndScoreSaved = false;
    }

    /// <summary>
    /// Add entry with playerName and score
    /// </summary>
    /// <param name="playerName"></param>
    /// <param name="score"></param>
    private void AddEntry(string playerName, int score)
    {
        // default is 4 -> will not be shown in table
        var placeForNewEntry = 4;
        
        // check where to add new entry
        for (var i = 0; i < _entryDictionary.Count; i++)
        {
            // check if new score is higher or equal to the entry
            // if yes, save index so that the new entry will be put there
            if (Convert.ToInt32(_entryDictionary[i][1]) <= score)
            {
                placeForNewEntry = i;
                break;
            }
        }

        // if placeForNewEntry was not updated in the for loop, set it to _entryDictionaryCount
        // --> either a new entry will be added/set active or placeForNewEntry will be 4
        if (placeForNewEntry == 4)
        {
            placeForNewEntry = _entryDictionary.Count;
        }

        // if new entry can be added as place 1, 2, 3 or 4, add it
        if (placeForNewEntry < 4)
        {
            // reorder the dictionary if necessary
            ReorderRecursively(placeForNewEntry, playerName, score.ToString());

            // update the displayed data
            for (var i = 0; i < _entryDictionary.Count; i++)
            {
                _entries[i].SetActive(true);
                _entries[i].transform.GetChild(0).GetComponent<Text>().text = _entryDictionary[i][0];
                _entries[i].transform.GetChild(1).GetComponent<Text>().text = _entryDictionary[i][1];
            }
        }
    }

    /// <summary>
    /// Saves the new data [name, score] to the dictionary at indexToUpdate
    /// If indexToUpdate already exists, the already existing data will be saved at indexToUpdate+1
    /// </summary>
    /// <param name="indexToUpdate"></param>
    /// <param name="name"></param>
    /// <param name="score"></param>
    private void ReorderRecursively(int indexToUpdate, string name, string score)
    {
        if (_entryDictionary.TryGetValue(indexToUpdate, out _data))
        {
            // if key already exists and next position is a valid highscore position, try to add entry in the next position
            if (indexToUpdate + 1 < 4)
            {
                ReorderRecursively(indexToUpdate + 1, _data[0], _data[1]);
            }
        }
        // add new entry
        List<string> temp = new List<string>();
        temp.Add(name);
        temp.Add(score);
       _entryDictionary[indexToUpdate] = temp;
    }
}
