﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Adds an entry to the highscore
/// </summary>
public class AddHighscoreEntry : MonoBehaviour
{
    [SerializeField] private Text _nameInput;
    private int _score;

    private void Update()
    {
        _score = GetComponent<UpdateScore>().score;
    }

    /// <summary>
    /// Save player name and player score, set newNameAndScoreSaved in GameManager to true
    /// </summary>
    public void SetParameters()
    {
        GameManager.Instance.playerName = _nameInput.text;
        GameManager.Instance.playerScore = _score;
        GameManager.Instance.newNameAndScoreSaved = true;
    }
}
