﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Updates and applies the settings
/// </summary>
public class AdjustSettings : MonoBehaviour
{
    [Header("Life Settings")]
    [SerializeField] private List<GameObject> _lifePrefabs;
    [SerializeField] private Slider _lifeSlider;
    private float _currentLifeValue;

    [Header("Speed Settings")]
    [SerializeField] private Text _speedButtonText;
    [SerializeField] private GameObject _groundSphere;
    private string _currentSpeedMultiplier;

    [Header("Difficulty Settings")] 
    [SerializeField] private List<GameObject> _difficultyOptions;
    private int _currentMaxNumberOfRange;

    [Header("Points Per Correct Answer Info")] 
    [SerializeField] private Text _pointsDisplay;
    private int _currentPointsPerCorrectAnswer;

    private void Awake()
    {
        // life settings
        _lifeSlider.value = GameManager.Instance.playerLivesOnStart;
        _currentLifeValue = _lifeSlider.value;
        ShowPlayerLives(_currentLifeValue);

        // speed settings
        _speedButtonText.text = GetNextMultiplier(GameManager.Instance.groundMovementSpeed);
        _currentSpeedMultiplier = _speedButtonText.text;

        // difficulty settings
        _currentMaxNumberOfRange = GameManager.Instance.maxNumberOfTaskNumberRange;
        var optionToActivate = CheckWhichToggleToActivate(_currentMaxNumberOfRange);
        foreach (var option in _difficultyOptions)
        {
            if (option == optionToActivate)
            {
                option.GetComponent<Toggle>().isOn = true;
            }
            else
            {
                option.GetComponent<Toggle>().isOn = false;
            }
        }
        
        // points per correct answer display
        _currentPointsPerCorrectAnswer = GameManager.Instance.pointsPerCorrectAnswer;
        _pointsDisplay.text = _currentPointsPerCorrectAnswer.ToString();
    }

    /// <summary>
    /// Update the display of lives in the settings
    /// </summary>
    /// <param name="lives"></param>
    private void ShowPlayerLives(float lives)
    {
        for (int i = 0; i < _lifePrefabs.Count; i++)
        {
            if (i < (int) lives)
            {
                _lifePrefabs[i].SetActive(true);
            }
            else
            {
                _lifePrefabs[i].SetActive(false);
            }
        }    
    }

    /// <summary>
    /// Update the speed multiplier button text and points per correct answer depending on the chosen speed
    /// </summary>
    public void UpdateButtonText()
    {
        switch (_speedButtonText.text)
        {
            // if text was x 1 before, the multiplier was x 3 before and the next should be x 2
            case "x 1":
                _speedButtonText.text = "x 2";
                UpdatePointsPerCorrectAnswerDisplay(-2);
                break;
            
            case "x 2":
                _speedButtonText.text = "x 3";
                UpdatePointsPerCorrectAnswerDisplay(1);
                break;
            
            case "x 3":
                _speedButtonText.text = "x 1";
                UpdatePointsPerCorrectAnswerDisplay(1);
                break;
        }
    }

    /// <summary>
    /// Sets the speed determined by multiplier
    /// </summary>
    /// <param name="multiplier"></param>
    /// <returns></returns>
    private static int SetSpeed(string multiplier)
    {
        switch (multiplier)
        {
            // if "x 1" is the button text, "x 1" will be the next state -> current state: "x 3"
            case "x 1":
                return 30;
            
            case "x 2":
                return 15;
            
            case "x 3":
                return 22;
            
            default:
                return 15;
        }
    }

    /// <summary>
    /// Returns the next multiplier according to current speed settings
    /// </summary>
    /// <param name="speed"></param>
    /// <returns></returns>
    private static string GetNextMultiplier(float speed)
    {
        switch (speed)
        {
            // if current speed is the lowest possible value, the next value will be x 2 -> button text is x 2
            case 15f:
                return "x 2";
            
            case 22f:
                return "x 3";
            
            case 30f:
                return "x 1";
            
            default:
                return "x 2";
        }
    }

    /// <summary>
    /// Update the upper range limit for the summands according to which toggle is activated
    /// and update points per correct answer that depend on the set difficulty
    /// </summary>
    public void UpdateMaxNumberOfRange()
    {
        foreach (var toggle in _difficultyOptions)
        {
            if (toggle.GetComponent<Toggle>().isOn & toggle.name.Contains("10"))
            {
                switch (_currentMaxNumberOfRange)
                {
                    case 25:
                        UpdatePointsPerCorrectAnswerDisplay(-1);
                        break;
                    case 50:
                        UpdatePointsPerCorrectAnswerDisplay(-2);
                        break;
                }

                _currentMaxNumberOfRange = 10;
            }
            else if (toggle.GetComponent<Toggle>().isOn & toggle.name.Contains("25"))
            {
                switch (_currentMaxNumberOfRange)
                {
                    case 10:
                        UpdatePointsPerCorrectAnswerDisplay(1);
                        break;
                    case 50:
                        UpdatePointsPerCorrectAnswerDisplay(-1);
                        break;
                }

                _currentMaxNumberOfRange = 25;
            }
            else if (toggle.GetComponent<Toggle>().isOn & toggle.name.Contains("50"))
            {
                switch (_currentMaxNumberOfRange)
                {
                    case 10:
                        UpdatePointsPerCorrectAnswerDisplay(2);
                        break;
                    case 25:
                        UpdatePointsPerCorrectAnswerDisplay(1);
                        break;
                }

                _currentMaxNumberOfRange = 50;
            }
        }
        // save new maxNumber to GameManager
        GameManager.Instance.SetMaxNumberOfTaskNumberRange(_currentMaxNumberOfRange);
    }

    /// <summary>
    /// Update points per correct answer by adding increment, display the updated value and save
    /// it to GameManager
    /// </summary>
    /// <param name="increment"></param>
    private void UpdatePointsPerCorrectAnswerDisplay(int increment)
    {
        _currentPointsPerCorrectAnswer += increment;
        _pointsDisplay.text = _currentPointsPerCorrectAnswer.ToString();
        GameManager.Instance.SetPointsPerCorrectAnswer(_currentPointsPerCorrectAnswer);
    }

    /// <summary>
    /// Checks which toggle should be enabled according to maxRangeNumber
    /// </summary>
    /// <param name="maxRangeNumber"></param>
    /// <returns></returns>
    private GameObject CheckWhichToggleToActivate(int maxRangeNumber)
    {
        switch (maxRangeNumber)
        {
            case 10:
                return _difficultyOptions[0];
            
            case 25:
                return _difficultyOptions[1];
            
            case 50:
                return _difficultyOptions[2];
            
            default:
                return _difficultyOptions[0];
        }
    }

    private void Update()
    {
        // life settings
        // update currentLifeValue, displayed player lives and GameManager saved life value if slider value changed
        if ((int) _currentLifeValue != (int) _lifeSlider.value)
        {
            _currentLifeValue = _lifeSlider.value;
            ShowPlayerLives(_currentLifeValue);
            GameManager.Instance.SetPlayerLives((int) _currentLifeValue);
        }

        // speed settings
        // update currentSpeedMultiplier, sphere rotation speed and GameManager saved speed value if speed button was pressed
        if (_currentSpeedMultiplier != _speedButtonText.text)
        {
            _currentSpeedMultiplier = _speedButtonText.text;
            int speed = SetSpeed(_currentSpeedMultiplier);
            _groundSphere.GetComponent<GroundMovement>().speed = speed;
            GameManager.Instance.SetGroundMovementSpeed(speed);
        }
        
    }
}
