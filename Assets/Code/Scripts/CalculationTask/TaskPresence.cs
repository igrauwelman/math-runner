﻿using UnityEngine;

/// <summary>
/// Activates and deactivates choice GameObjects, color codes whether answer was correct on collision with player
/// </summary>
public class TaskPresence : MonoBehaviour
{
    private GameObject _player;
    public bool isCorrect;
    private TaskGenerator _taskGenerator;
    
    public bool IsCorrect 
    { 
        get
            { return isCorrect;}
        set
            { isCorrect = value;}
    }

    private void Start()
    {
        _player = GameObject.FindWithTag("Player");
        _taskGenerator = FindObjectOfType<TaskGenerator>();
        // deactivate choice object on start
        gameObject.SetActive(false);
    }

    
    /// <summary>
    /// Update score or player lives and color code given answer if player enters
    /// </summary>
    private void OnTriggerEnter(Collider other)
    {
        // if player chose the correct answer ...
        if (other.gameObject == _player & isCorrect)
        {
            // ... update score depending on set speed and task difficulty
            switch (GameManager.Instance.groundMovementSpeed)
            {
                case 15f:
                    GameManager.Instance.UpdateScore(1);
                    break;
                
                case 22f:
                    GameManager.Instance.UpdateScore(2);
                    break;
                
                case 30f:
                    GameManager.Instance.UpdateScore(3);
                    break;
            }

            switch (GameManager.Instance.maxNumberOfTaskNumberRange)
            {
                case 25:
                    GameManager.Instance.UpdateScore(1);
                    break;
                
                case 50:
                    GameManager.Instance.UpdateScore(2);
                    break;
            }
            
            // ... display the result and make result text color green
            _taskGenerator.taskText[2].text = _taskGenerator.result.ToString();
            _taskGenerator.taskText[2].color = Color.green;
        }
        // if player chose one of the incorrect answers ...
        else if (other.gameObject == _player & !isCorrect)
        {
            // ... subtract one player life
            GameManager.Instance.playerLives -= 1;
            // ... display the result and make result text color red
            _taskGenerator.taskText[2].text = _taskGenerator.result.ToString();
            _taskGenerator.taskText[2].color = Color.red;
        }
       
    }

    /// <summary>
    /// Deactivate choice GameObject, reset correct answer and start the next task generation iteration
    /// </summary>
    private void OnTriggerExit(Collider other)
    {
        gameObject.SetActive(false);
        isCorrect = false;
        _taskGenerator.StartCoroutine("GenerateAndDisplayTask");
    }
}
