﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.GameObject;
using Random = UnityEngine.Random;

/// <summary>
/// Generates the calculation task and the answer choices randomly and displays them
/// </summary>
public class TaskGenerator : MonoBehaviour
{
    // task GameObject, task text and its components
    private GameObject _calculationTaskObject;
    [HideInInspector] public SortedList<int, Text> taskText = new SortedList<int, Text>();
    private int _firstNumber;
    private int _secondNumber;
    [HideInInspector] public int result;
    private Color _originalColor; // color of the task text to reset to after color coding result
    
    // choice texts
    private List<Text> _choicesTexts = new List<Text>();
    
    // upper number range limit for task summands
    private int _maxNumber;
    
    private int _randomIndex;
    
    private void OnEnable()
    {
        // find task text objects and save them
        var taskobjects = FindGameObjectsWithTag("CalculationTask");
        for (int i = 0; i < taskobjects.Length; i++)
        {
            switch (taskobjects[i].name)
            {
                case "FirstNumber":
                    taskText.Add(0, taskobjects[i].GetComponent<Text>());
                    break;
                
                case "SecondNumber":
                    taskText.Add(1, taskobjects[i].GetComponent<Text>());
                    break;
                
                case "Result":
                    taskText.Add(2, taskobjects[i].GetComponent<Text>());
                    break;
            }
        }

        foreach (var part in taskText)
        {
            Debug.LogError(part.Key + ": " + part.Value);
        }

        // find choice task objects and save them
        var choiceobjects = FindGameObjectsWithTag("ResultChoice");
        for (int i = 0; i < choiceobjects.Length; i++)
        {
            _choicesTexts.Add(choiceobjects[i].GetComponent<Text>());
        }

        _calculationTaskObject = Find("CalculationTask").gameObject;
        _originalColor = taskText[2].color;

        _maxNumber = GameManager.Instance.maxNumberOfTaskNumberRange;
    }

    private void Start()
    {
        // do not display the task on start
        _calculationTaskObject.transform.GetChild(0).gameObject.SetActive(false);
        StartCoroutine(GenerateAndDisplayTask());
    }

    /// <summary>
    /// Generates the task and incorrect choices randomly and displays them
    /// </summary>
    /// <returns></returns>
    public IEnumerator GenerateAndDisplayTask()
    {
        // generate task and calculate result
        _firstNumber = Random.Range(1, _maxNumber);
        _secondNumber = Random.Range(1, _maxNumber);
        result = _firstNumber + _secondNumber;

        _randomIndex = Random.Range(0, _choicesTexts.Count);
        var falseChoiceIndices = new List<int>(); // to later check whether false choices are the same
        
        for (int i = 0; i < _choicesTexts.Count; i++)
        {
            // generate and display incorrect choices at indices != randomIndex
            if (i != _randomIndex)
            {
                DisplayFalseChoices(_choicesTexts[i], result);
                falseChoiceIndices.Add(i);
            }
            // display correct choice at random index
            else
            {
                DisplayCorrectChoice(_choicesTexts[_randomIndex], result.ToString());
            }
        }

        // if incorrect choices are the same, generate another incorrect choice
        while (_choicesTexts[falseChoiceIndices[0]].text == _choicesTexts[falseChoiceIndices[1]].text)
        {
            DisplayFalseChoices(_choicesTexts[falseChoiceIndices[0]], result);
        }
        
        // wait before deactivating and resetting the task 
        yield return new WaitForSeconds(1f);
        _calculationTaskObject.transform.GetChild(0).gameObject.SetActive(false);
        taskText[2].text = "?";
        taskText[2].color = _originalColor;

        // wait before displaying the (new) task (dependent on movement speed)
        if (GameManager.Instance.groundMovementSpeed <= 25)
        {
            yield return new WaitForSeconds(3f);
        }
        else
        {
            yield return new WaitForSeconds(1f);  // wait shorter for the highest speed setting
        }
        
        // display task
        taskText[0].text = _firstNumber.ToString();
        taskText[1].text = _secondNumber.ToString();
        _calculationTaskObject.transform.GetChild(0).gameObject.SetActive(true);
    }

    /// <summary>
    /// Update choiceText with resultText and set it to be the correct answer
    /// </summary>
    /// <param name="choiceText"></param>
    /// <param name="resultText"></param>
    private static void DisplayCorrectChoice(Text choiceText, string resultText)
    {
        choiceText.text = resultText;
        choiceText.transform.parent.transform.parent.gameObject.GetComponent<TaskPresence>().isCorrect = true;
    }

    
    /// <summary>
    /// Update choiceText with result +/- 5 and set it to be an incorrect answer
    /// </summary>
    /// <param name="choiceText"></param>
    /// <param name="result"></param>
    private void DisplayFalseChoices(Text choiceText, int result)
    {
        choiceText.text = (Mathf.Abs(this.result + Random.Range(5 * (-1), 5))).ToString();
        // make sure the resultText is not chosen as a false choiceText coincidentally
        while (choiceText.text == this.result.ToString())
        {
            choiceText.text = (Mathf.Abs(this.result + Random.Range(5 * (-1), 5))).ToString();
        }
        choiceText.transform.parent.transform.parent.gameObject.GetComponent<TaskPresence>().isCorrect = false;

    }
}
