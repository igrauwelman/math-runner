﻿using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Manages the third person camera roll
/// </summary>
public class PlayerCamera : MonoBehaviour
{
    private Transform _player;
    private Transform _camera;

    // offset needed for third person camera roll
    private float _yOffset;
    private float _zOffset;
    
    void Start()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(1))
        {
            _player = GameObject.Find("Player").transform;
            _camera = GameObject.Find("Main Camera").transform;

            _yOffset = _camera.position.y - _player.position.y;
            _zOffset = _camera.position.z - _player.position.z;
        }
    }
    
    void LateUpdate()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(1))
        {
            // follow the player
            transform.position = new Vector3(0f, _player.position.y + _yOffset, _player.position.z + _zOffset);
        }
    }
}
