﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Updates the player's score
/// </summary>
public class UpdateScore : MonoBehaviour
{
    [SerializeField] private Text _scoreText;
    public int score = 0;

    private void Update()
    {
        if (_scoreText != null)
        {
            _scoreText.text = "Score: " + GameManager.Instance.score.ToString(); // display the score
            score = GameManager.Instance.score; // update score value in GameManager
        }
    }
}
