﻿using UnityEngine;

/// <summary>
/// Manages the player movement
/// </summary>
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _speed = 10f;

    private void Update()
    {
        // in-game / "real" player
        if (gameObject.CompareTag("Player"))
        {
            var horizontalMovement = Input.GetAxis("Horizontal") * _speed;
            transform.Translate(new Vector3(horizontalMovement, 0f, 0f) * Time.deltaTime, Space.World);
            
            if (transform.position.x > 3f)
            {
                transform.position = new Vector3(3f, transform.position.y, transform.position.z);
            }

            if (transform.position.x < -3f)
            {
                transform.position = new Vector3(-3f, transform.position.y, transform.position.z);

            }
        }
        // how-to-play player
        else
        {
            var horizontalMovement = Input.GetAxis("Horizontal") * _speed * 0.5f;
            transform.Translate(new Vector3(horizontalMovement, 0f, 0f) * Time.deltaTime, Space.World);
            
            if (transform.position.x > -1.3f)
            {
                transform.position = new Vector3(-1.3f, transform.position.y, transform.position.z);
            }

            if (transform.position.x < -2.3f)
            {
                transform.position = new Vector3(-2.3f, transform.position.y, transform.position.z);

            }
        }
       
    }
}
