﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Updates the display of the player lives
/// </summary>
public class PlayerLives : MonoBehaviour
{
    private int _lives;
    [SerializeField] private List<GameObject> _lifePrefabs;

    private void Update()
    {
        _lives = GameManager.Instance.playerLives;
        
        for (var i = 0; i < _lifePrefabs.Count; i++)
        {
            if (i < _lives)
            {
                _lifePrefabs[i].SetActive(true);
            }
            else
            {
                _lifePrefabs[i].SetActive(false);
            }
        }
    }
}
