﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    [Header("Player")]
    public int playerLivesOnStart = 5;
    public int playerLives;
    public int pointsPerCorrectAnswer = 1;
    public int score = 0;
    
    [Header("HighScore")]
    public string playerName;
    public int playerScore;
    public int highscore;
    public bool newNameAndScoreSaved = false;
    [HideInInspector] public Dictionary<int, List<string>> highscoreEntries;
    
    [Header("Difficulty")]
    public float groundMovementSpeed = 15f;
    public int maxNumberOfTaskNumberRange = 10;
    
    [Header("Scene Management")]
    public int lastLoadedMenuIndex;
    public bool lastSceneWasGame;

    public static GameManager Instance => _instance;

    
    /// <summary>
    /// Sets playerLivesOnStart and playerLives to lives
    /// </summary>
    /// <param name="lives"></param>
    public void SetPlayerLives(int lives)
    { 
        playerLivesOnStart = lives;
        playerLives = playerLivesOnStart;
    }

    
    /// <summary>
    /// Sets groundMovementSpeed to speed
    /// </summary>
    /// <param name="speed"></param>
    public void SetGroundMovementSpeed(float speed)
    {
        groundMovementSpeed = speed;
    }
    
    
    /// <summary>
    /// Sets maxNumberOfTaskNumberRange to number
    /// </summary>
    /// <param name="number"></param>
    public void SetMaxNumberOfTaskNumberRange(int number)
    {
        maxNumberOfTaskNumberRange = number;
    }

    /// <summary>
    /// Sets pointsPerCorrectAnswer to number
    /// </summary>
    /// <param name="number"></param>
    public void SetPointsPerCorrectAnswer(int number)
    {
        pointsPerCorrectAnswer = number;
    }
    
    /// <summary>
    /// Updates the player score by adding points
    /// </summary>
    /// <param name="points"></param>
    public void UpdateScore(int points)
    {
        score += points;
    }

    
    /// <summary>
    /// Saves menu by saving the scene's index (build index)
    /// </summary>
    /// <param name="index"></param>
    public void SaveMenu(int index)
    {
        lastLoadedMenuIndex = index;
    }

    /// <summary>
    /// Reset values playerName to null, playerScore to 0, playerLives to playerLivesOnStart, score to 0 and newNameAndScoreSaved to false
    /// </summary>
    public void Reset()
    { 
        playerName = null;
        playerScore = 0;
        playerLives = playerLivesOnStart;
        score = 0;
        newNameAndScoreSaved = false;
    }
    
    private void Awake()
    {
        // make GameManager accessible across scenes
        DontDestroyOnLoad(this);
        
        // Singleton
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        // last loaded menu is start menu on start
        lastLoadedMenuIndex = 0;
        // last scene was not game scene on start
        lastSceneWasGame = false;
        
        // if HighScore key already exists in PlayerPrefs, get saved highscore
        if (PlayerPrefs.HasKey("HighScore"))
        {
            highscore = PlayerPrefs.GetInt("HighScore");
        }
        // if HighScore key does not exist in PlayerPrefs, create key and store highscore
        else
        {
            PlayerPrefs.SetInt("HighScore", highscore);
        }
        
        // create a dictionary for the highscore entries
        highscoreEntries = new Dictionary<int, List<string>>();

        // if HighScoreDictionary key already exists in PlayerPrefs, get saved highscore entries
        if (PlayerPrefs.HasKey("HighScoreDictionary"))
        {
            // dictionary saved as "NAME1 - SCORE1, NAME2 - SCORE2, ..."
            string savedDictionary = PlayerPrefs.GetString("HighScoreDictionary");
            string[] savedDictionaryEntries = savedDictionary.Split(','); // split to single entries NAME - SCORE

            for (int i = 0; i < savedDictionaryEntries.Length - 1; i++)
            {
                string[] splitEntry = savedDictionaryEntries[i].Split('-'); // split to NAME and SCORE
                List<string> tempEntryList = new List<string>();
                tempEntryList.Add(splitEntry[0]);
                tempEntryList.Add(splitEntry[1]);
                highscoreEntries[i] = tempEntryList; // add NAME and SCORE values as values to dictionary at key i
            }
        }
    }

    private void Update()
    {
        // check whether player lost their last life in game scene
        if (playerLives <= 0 & SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(1))
        { 
            StartCoroutine(DisplayLastMistakeAndLoadGameOver());
        }
    }

    /// <summary>
    /// Wait before loading game over menu to display last mistake / correct answer
    /// </summary>
    /// <returns></returns>
    private IEnumerator DisplayLastMistakeAndLoadGameOver()
    {
        yield return new WaitForSeconds(1f);
        
        // save new highscore if score is greater than highscore or highscore is 0
        if (score > highscore || highscore == 0)
        {
            highscore = score;
            PlayerPrefs.SetInt("HighScore", highscore);
        }

        // last scene is game and last menu is game over menu
        lastSceneWasGame = true;
        SaveMenu(2);
            
        SceneManager.LoadScene(2);
        StopAllCoroutines();
    }

    /// <summary>
    /// Save the highscore table
    /// </summary>
    private void OnApplicationQuit()
    {
        string highscoreString = "";
        foreach (var entry in highscoreEntries)
        {
            highscoreString = highscoreString + entry.Value[0] + "-" + entry.Value[1] + ",";
        }
        PlayerPrefs.SetString("HighScoreDictionary", highscoreString);
    }
}
