﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Manages scene changes and UI interactions
/// </summary>
public class SceneChangeManager : MonoBehaviour
{
    [SerializeField] private GameObject _nameInputUI; // highscore name input field
    [SerializeField] private GameObject _inGameMenu; // in-game menu or how to play
    
    // in-game objects to deactivate if game is paused
    private GameObject _player;
    private GameObject _ground;
    private GameObject _task;
    
    // in-game pause button
    private GameObject _pauseButton;
    
    // start menu objects to adjust if how to play is opened
    private GameObject _jammoVisual;
    private PlayerMovement _movementScript;
    private Vector3 _startMenuPlayerPosition;
    private GameObject _asteroidVisual;
    // start menu objects to deactivate if how to play is opened
    private List<GameObject> _startMenuObjects;
    

    private void Awake()
    {
        if (_nameInputUI != null)
        {
            // allow player input after game over (if no entry was made yet)
            if (GameManager.Instance.lastSceneWasGame)
            {
                _nameInputUI.SetActive(true);
            }
            else
            {
                _nameInputUI.SetActive(false);
            }
        }

        if (_inGameMenu != null)
        {
            // deactivate in-game menu / how to play on start
            _inGameMenu.SetActive(false);
        }

        // save in-game objects to deactivate if game is paused
        if (GameObject.Find("Player") != null)
        {
            _player = GameObject.Find("Player");
        }

        if (GameObject.Find("Ground") != null)
        {
            _ground = GameObject.Find("Ground");
        }

        if (GameObject.Find("CalculationTask") != null)
        {
            _task = GameObject.Find("CalculationTask");
        }

        _pauseButton = gameObject.transform.GetChild(1).gameObject;

        // if current scene is start menu scene
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByBuildIndex(0))
        {
            // find "visual" GameObjects
            if (GameObject.Find("Visuals") != null)
            {
                _jammoVisual = GameObject.Find("Visuals").transform.GetChild(0).gameObject;
                _movementScript = _jammoVisual.GetComponent<PlayerMovement>();
                _movementScript.enabled = false; // disable player movement script in start menu
                _startMenuPlayerPosition = _jammoVisual.transform.position;
                _asteroidVisual = GameObject.Find("Visuals").transform.GetChild(1).gameObject;
            }
           
            // save objects to deactivate if how to play is opened
            _startMenuObjects = new List<GameObject>();
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                _startMenuObjects.Add(gameObject.transform.GetChild(i).gameObject);
            }
        }
    }

    /// <summary>
    /// Start game by loading the respective scene
    /// Reset GameManager values and set lastSceneWasGame to true
    /// </summary>
    public void StartGame()
    {
        Time.timeScale = 1f;
        GameManager.Instance.Reset();
        GameManager.Instance.lastSceneWasGame = true;
        SceneManager.LoadScene(1);
    }

    /// <summary>
    /// Load start menu by loading the respective scene
    /// Save menu in GameManager and set lastSceneWasGame to false
    /// </summary>
    public void LoadStartMenu()
    {
        Time.timeScale = 1f;
        GameManager.Instance.SaveMenu(0);
        GameManager.Instance.lastSceneWasGame = false;
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Load the last opened menu by loading the respective scene
    /// </summary>
    public void BackToMenu()
    {
        switch (GameManager.Instance.lastLoadedMenuIndex)
        {
            // load start menu
            case 0:
                LoadStartMenu();
                break;
            
            // load game (last menu: in-game menu)
            case 1:
                SceneManager.LoadScene(1);
                break;
            
            // load game over menu
            case 2:
                SceneManager.LoadScene(2);
                break;
        }

        if (_inGameMenu != null)
        {
            // if in-game menu is active (last menu was in-game menu), pause game
            if (_inGameMenu.activeSelf)
            {
                Time.timeScale = 0f;
            }
        }
        // if there is no _inGameMenu (last menu was not in-game menu), unpause game
        else
        {
            Time.timeScale = 1f;
        }
    }

    /// <summary>
    /// Shows the highscore table by loading the respective scene
    /// </summary>
    public void ShowHighScore()
    {
        if (GameManager.Instance.lastSceneWasGame & !GameManager.Instance.newNameAndScoreSaved)
        {
            GameManager.Instance.lastSceneWasGame = true;
        }
        else
        {
            // set this to false so that new score entry input field is no longer active if entry was already made
            GameManager.Instance.lastSceneWasGame = false;
        }
        SceneManager.LoadScene(4);
    }

    
    /// <summary>
    /// Open the settings by loading the respective scene
    /// </summary>
    public void OpenSettings()
    {
        Time.timeScale = 1f;
        
        if (GameManager.Instance.lastSceneWasGame & !GameManager.Instance.newNameAndScoreSaved)
        {
            GameManager.Instance.lastSceneWasGame = true;
        }
        else
        {
            GameManager.Instance.lastSceneWasGame = false;
        }
        
        GameManager.Instance.lastSceneWasGame = false;
        SceneManager.LoadScene(3);
    }

    /// <summary>
    /// Pauses the game by setting Time.timeScale to zero
    /// Deactivates in-game objects and activates the in-game menu
    /// Saves last menu (here: game), deactivates the pause button and stops task generation
    /// </summary>
    public void PauseGame()
    {
        GameManager.Instance.SaveMenu(1);
        _pauseButton.SetActive(false);
        StopCoroutine(_task.GetComponent<TaskGenerator>().GenerateAndDisplayTask());
        _task.transform.GetChild(0).gameObject.SetActive(false);
        for (int i = 0; i < _ground.transform.GetChild(1).transform.GetChild(1).transform.childCount; i++)
        {
            _ground.transform.GetChild(1).transform.GetChild(1).transform.GetChild(i).gameObject.SetActive(false);
        }
        _player.SetActive(false);
        _ground.SetActive(false);
        _task.SetActive(false);
        if (_inGameMenu != null)
        {
            _inGameMenu.SetActive(true);
        }
        Time.timeScale = 0f;
    }

    /// <summary>
    /// Unpauses the game by setting Time.timeScale to 1
    /// Activates in-game objects and deactivates the in-game menu
    /// Activates the pause button and starts task generation
    /// </summary>
    public void ResumeGame()
    {
        _pauseButton.SetActive(true);
        StartCoroutine(_task.GetComponent<TaskGenerator>().GenerateAndDisplayTask());
        _player.SetActive(true);
        _ground.SetActive(true);
        _task.SetActive(true); 
        if (_inGameMenu != null)
        {
            _inGameMenu.SetActive(false);
        }
        Time.timeScale = 1f;
    }

    /// <summary>
    /// Opens the how to play explanation by activating the how to play GameObject and deactivating start menu objects
    /// Adjusts visuals
    /// </summary>
    public void OpenHowTo()
    {
        _jammoVisual.transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f);
        _movementScript.enabled = true;
        _asteroidVisual.transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f);
        _inGameMenu.SetActive(true);
        foreach (var menuObject in _startMenuObjects)
        {
            menuObject.SetActive(false);
        }
    }

    /// <summary>
    /// Closes the how to play explanation by deactivating the how to play GameObject and activating start menu objects
    /// Adjusts visuals
    /// </summary>
    public void CloseHowTo()
    {
        _jammoVisual.transform.localScale += new Vector3(0.5f, 0.5f, 0.5f);
        _movementScript.enabled = false;
        _jammoVisual.transform.position = _startMenuPlayerPosition;
        _asteroidVisual.transform.localScale += new Vector3(0.5f, 0.5f, 0.5f); 
        _inGameMenu.SetActive(false);
        foreach (var menuObject in _startMenuObjects)
        {
            menuObject.SetActive(true);
        }
    }
}
