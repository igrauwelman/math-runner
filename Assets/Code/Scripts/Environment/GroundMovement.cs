﻿using UnityEngine;

/// <summary>
/// Makes the ground move
/// </summary>
public class GroundMovement : MonoBehaviour
{
    public float speed;
    
    void Update()
    {
        speed = GameManager.Instance.groundMovementSpeed;
        
        // if this = sphere in the settings, rotate
        if (gameObject.name.Contains("Sphere"))
        {
           transform.Rotate(new Vector3(speed / 60, 0f , 0f), Space.World);
        }
        // if this = ground, move "back"
        else
        {
            transform.Translate(Vector3.back * speed * Time.deltaTime);
        }
    }
}
