﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Respawns the ground tile for an endless runner effect and sets choice GameObjects active if part of the ground tile
/// </summary>
public class TriggerGroundRespawn : MonoBehaviour
{
    private Transform _ground;
    private List<TaskPresence> _choices = new List<TaskPresence>();
    private GameObject _choiceSection;

    private void OnEnable()
    {
        // if ground tile is the ground variant with choices, save choice GameObjects
        if (gameObject.transform.parent.name.Contains("WithChoices"))
        {
            _choiceSection = GameObject.Find("GroundSectionWithChoices");
            for (int i = 0; i < _choiceSection.GetComponentsInChildren<TaskPresence>().Length; i++)
            {
                _choices.Add(_choiceSection.GetComponentsInChildren<TaskPresence>()[i]);
            }
        }
    }

    /// <summary>
    /// Respawn the ground tile and activates choice GameObjects if they are part of the ground tile
    /// if player enters
    /// </summary>
    private void OnTriggerEnter(Collider other)
       {
           _ground = transform.parent.transform;
           if (other.gameObject.name.Contains("Player"))
           {
               _ground.transform.position += new Vector3(0f, 0f, 208.5f);
           }

           foreach (var choice in _choices)
           {
               choice.gameObject.SetActive(true);
           }
       }
}
